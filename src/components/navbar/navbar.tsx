import React from 'react';
import {useDispatch} from "react-redux";
import {handleActiveAction} from "../../actions/ui";
import useStateRedux from "../../hooks/useStateRedux";

const Navbar = () => {
  const dispatch = useDispatch()
  const {actions} = useStateRedux('UI')
  
  const handleAction =(id: string | number) => dispatch(handleActiveAction(id))
  
  return <div className='journal__navbar beauty-scroll'>
    {actions.data.map(({id, name}: any) =>
      <div key={id} className={`journal__navbar-item ${id === actions.active ? 'active' : ''}`}>
        <div onClick={() => handleAction(id)}>
          {name}
        </div>
      </div>
    )}
  </div>
};

export default Navbar;
