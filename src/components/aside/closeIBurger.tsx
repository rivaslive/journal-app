import {LeftCircleFilled, RightCircleFilled} from "@ant-design/icons";
import React from "react";

const CloseBurger = ({setShow, show}: { setShow: () => void, show: boolean }) => (
  <div
    className={`journal__burger-menu ${show ? 'burger-open' : 'burger-close'}`}
    onClick={setShow}
  >
    {show ? <LeftCircleFilled className='animate__animated animate__rotateIn animate__faster'/>
      : <RightCircleFilled className='animate__animated animate__rotateIn animate__faster'/>}
  </div>
)
export default CloseBurger
