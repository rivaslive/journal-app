import moment from "moment";
import {BarLeft} from "./MenuLeft";
import {Button, Tree, Typography} from 'antd';
import CloseBurger from "./closeIBurger";
import React, {useEffect, useState} from 'react';
import useStateRedux from "../../hooks/useStateRedux";
import {transformNoteList} from "../../helpers/transform";
import {useDispatch} from "react-redux";
import {changeActiveNote} from "../../actions/note";
import {PlusOutlined} from "@ant-design/icons";
import {handleActiveAction} from "../../actions/ui";

const {Title, Text} = Typography;

const Aside = () => {
  const dispatch = useDispatch();
  const [show, setShow] = useState(true)
  const [treeData, setTree] = useState([])
  const {notes, active} = useStateRedux('note')
  const [animationClose, setAnimationClose] = useState(false)
  
  useEffect(() => {
    setTree(transformNoteList(notes))
  }, [notes, active, setTree])
  
  const handleShow = () => {
    setAnimationClose(!animationClose);
    if (show) setTimeout(() => setShow(!show), 250);
    else setShow(!show);
  }
  
  const onSelect = (props: any) => dispatch(changeActiveNote(props[0]))
  const newNote = () => dispatch(handleActiveAction(2))
  
  return <div>
    <div className='journal__aside'>
      <CloseBurger setShow={handleShow} show={show}/>
      <BarLeft/>
      <div
        className={`journal__aside-menu ${animationClose ? 'close-animation' : ''} ${!show ? 'deactivate' : 'activate'}`}>
        <div className='journal__aside-menu-position'>
          <Text className='text-secondary font-size-12'>{moment().format('dddd DD, MMMM YYYY')}</Text>
          <Title level={5} style={{marginTop: 0}}>Today's stories</Title>
          
          <div className='mt-2'>
            <div className='mt-2'>
              <Tree
                showIcon
                onSelect={onSelect}
                treeData={treeData}
                selectedKeys={[active]}
              />
              {!treeData.length && <Button
	              size='large'
	              type='primary'
	              onClick={newNote}
	              icon={<PlusOutlined/>}
	              style={{width: '100%'}}
              >
	              Create Note
              </Button>}
            </div>
          </div>
        
        </div>
      </div>
    </div>
  </div>
}
export default Aside;
