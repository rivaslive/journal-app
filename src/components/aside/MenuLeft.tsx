import React from "react";
import {useDispatch} from "react-redux";
import {logoutApp} from "../../actions/auth";
import useStateRedux from "../../hooks/useStateRedux";
import {Avatar, Dropdown, Menu, Typography} from "antd";
import {ExclamationCircleOutlined} from "@ant-design/icons";

const {Text} = Typography;

export const BarLeft = () => {
  const dispatch = useDispatch();
  const {auth} = useStateRedux();
  const menu = (
    <Menu>
      <Menu.Item>
        <UserHead {...auth}/>
      </Menu.Item>
      <Menu.Item onClick={() => dispatch(logoutApp())}>
        <div style={{padding: '10px 0'}} className='text-danger'>
          Logout
        </div>
      </Menu.Item>
    </Menu>
  );
  
  return <div className='journal__aside-bar' style={{textAlign: 'center'}}>
    <div className='mt-1'>
      <img src={`${process.env.PUBLIC_URL}/logo.svg`} alt="Brand" width='45'/>
    </div>
    <ExclamationCircleOutlined style={{fontSize: 25}} className='journal__help-icon'/>
    <Dropdown overlay={menu}>
      <Avatar style={{cursor: 'pointer'}} className='mr-auto ml-auto' src={auth.avatar}/>
    </Dropdown>
  </div>
}

const UserHead = ({name, email}: any) => <div className='journal__aside-menu-head-user'>
  <Text style={{fontWeight: 'bold'}}>{name}</Text>
  <br/>
  <Text style={{color: 'rgba(100, 100, 100, .8)'}} className='font-size-12'>{email}</Text>
</div>
