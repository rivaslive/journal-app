import moment from "moment";
import NewImage from "./newImage";
import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {useForm} from "antd/es/form/Form";
import TextArea from "antd/es/input/TextArea";
import {Button, Form, Typography} from 'antd';
import {addNewNote, deleteNote, handleNote} from "../actions/note";

const {Text} = Typography;

const NoteScreen = ({current, ...props}: any) => {
  const [form] = useForm();
  const dispatch = useDispatch();
  const {title, date, image, id} = props;
  
  useEffect(() => {
    const {title, note} = props;
    current === 1 ? form.setFields([
      {name: ['title'], value: title},
      {name: 'note', value: note},
    ]) : form.setFields(_INIT_FIELDS)
  }, [props, form, current])
  
  const clickWrapper = () => {
    const values = form.getFieldsValue();
    if (!values.title) return
    if(!id) {
      console.log('click wrapper, create note...');
      const payload = {...values}
      console.log(payload);
      return dispatch(addNewNote(payload))
    }
    console.log('click wrapper, update note...');
    const payload = {...props, ...values}
    dispatch(handleNote(payload))
  }
  
  const removeNote = () => dispatch(deleteNote(id))
  
  return <div key={id} style={{height: '100%'}} className='animate__animated animate__fadeIn'>
    <Text
      className='text-secondary font-size-12'>
      {moment(date).format('dddd DD, MMMM YYYY')}
    </Text>
  
    {
      !!id && <>
	      <NewImage {...props} current={current}/>
	      <div style={{float: 'right'}}>
		      <Button type='text' danger onClick={removeNote}>Delete</Button>
		      <Button type='text' className='text-primary' onClick={clickWrapper}>Save</Button>
        </div>
      </>
    }
    <div onClick={clickWrapper} style={{zIndex: 1, height: '100%'}}>
      {(image && current === 1) && <img src={image} alt={title} className='note__image animate__animated animate__fadeInDownBig'/>}
      <div onClick={(e) => e.stopPropagation()}>
        <Form form={form}>
          <Form.Item name='title'>
            <TextArea
              autoSize
              bordered={false}
              className='note__title'
              placeholder="Insert title here"
            />
          </Form.Item>
          
          <Form.Item name='note' className='mt-3'>
            <TextArea
              autoSize
              allowClear
              bordered={false}
              className='note__content'
              placeholder="Insert content here"
            />
          </Form.Item>
        </Form>
      </div>
    </div>
  </div>
}

const _INIT_FIELDS = [
  {name: ['title'], value: ''},
  {name: 'note', value: ''},
]

export default NoteScreen;
