import {useDispatch} from "react-redux";
import {handleNote} from "../actions/note";
import {message, Modal, Upload} from "antd";
import {storage} from "../service/firebase";
import React, {useCallback, useEffect, useState} from 'react';
import {generateHashName} from "../helpers/generateUUID";
import {CloseOutlined, LoadingOutlined, PaperClipOutlined, PlusOutlined} from "@ant-design/icons";

const NewImage = ({id, title, image}: any) => {
  const dispatch = useDispatch();
  const [state, setState] = useState<any>({visible: false, image: null, loading: false, imgErr: false});
  
  useEffect(() => {
    setState((state: any) => ({...state, image}))
  }, [image])
  
  const addImage = useCallback(() => {
    dispatch(handleNote({image: state.image, id, title}))
    setState((state: any) => ({...state, visible: false}))
  }, [dispatch, setState, id, title, state.image])
  
  const handleOpen = () => setState({...state, visible: !state.visible});
  
  const onChange = (info: any) => {
    console.log(info);
    if (info.file.status === 'uploading') return setState({...state, loading: true, imgErr: false});
    if (info.file.status === 'error') return setState({...state, loading: false, imgErr: true});
    if (info.file.status === 'done') getBase64(info.file.originFileObj, (image: any) => {
      setState({...state, loading: false, imgErr: false, image: info.file.xhr});
    });
  }
  
  const customUpload = async ({onError, onSuccess, file}: any) => {
    const firebaseStorage = storage();
    const metadata = {contentType: 'image/jpeg'}
    const storageRef = await firebaseStorage.ref();
    const imageName = generateHashName();
    const imgFile = storageRef.child(`notes/${imageName}-${file.name}`);
    try {
      await imgFile.put(file, metadata);
      const url = await imgFile.getDownloadURL();
      setState({...state, image: url})
      onSuccess(null, url);
    } catch (e) {
      onError(e)
    }
  };
  
  return <>
    <PaperClipOutlined className='note__click' onClick={handleOpen}/>
    
    <Modal
      centered
      width={450}
      onOk={addImage}
      destroyOnClose
      onCancel={handleOpen}
      visible={state.visible}
      okText='Apply image'
      title='Upload image'
      bodyStyle={{borderRadius: 20}}
      cancelButtonProps={{style: {display: 'none'}}}
      okButtonProps={{size: 'large', form: 'new-note', style: {marginBottom: 10}}}
    >
      <Upload
        {...propsDraw}
        onChange={onChange}
        beforeUpload={beforeUpload}
        customRequest={customUpload}
        className={`note__avatar-uploader ${state.imgErr ? 'note__avatar-uploader-err' : ''}`}
      >
        {state.image && !state.loading ? <img src={state.image} alt="avatar" className='note__avatar'/> :
          <UploadButton loading={state.loading} error={state.imgErr}/>}
      </Upload>
    </Modal>
  </>
}

const UploadButton = ({loading, error}: any) => (
  <div>
    {loading ? <LoadingOutlined/> : error ? <CloseOutlined/> : <PlusOutlined/>}
    <div style={{marginTop: 8}}>{!error ? 'Upload' : 'Error'}</div>
  </div>
);

function getBase64(img: any, callback: any) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file: any) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) message.error('You can only upload JPG/PNG file!');
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) message.error('Image must smaller than 2MB!');
  return isJpgOrPng && isLt2M;
}

const propsDraw = {
  multiple: true,
  showUploadList: false,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
};

export default NewImage;
