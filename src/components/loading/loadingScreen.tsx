import React from 'react';
import {Spin} from "antd";

const LoadingScreen = (props: IProps) => {
  return <div style={{
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100vh',
    backgroundColor: 'white',
    zIndex: 1000,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }}>
    <Spin size="large" />
  </div>
}

interface IProps {

}

export default LoadingScreen;
