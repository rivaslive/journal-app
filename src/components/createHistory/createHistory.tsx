import './_styles.scss';
import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {useForm} from "antd/es/form/Form";
import {addNewNote} from "../../actions/note";
import {PlusOutlined} from "@ant-design/icons";
import {Button, Form, Input, Modal} from 'antd';

const CreateHistory = () => {
  const [state, setState] = useState({visible: false, loading: false})
  
  const handleOpen = () => setState({...state, visible: !state.visible})
  const dispatch = useDispatch();
  
  const addNote = (payload: any) => {
    dispatch(addNewNote(payload))
    setState({...state, visible: false})
  }
  
  return <div className='history__create-history'>
    <Button
      size='large'
      type='primary'
      onClick={handleOpen}
      icon={<PlusOutlined/>}
    >
      Note
    </Button>
    <NewNoteModal
      centered
      width={450}
      onOk={addNote}
      destroyOnClose
      onCancel={handleOpen}
      visible={state.visible}
      okText='Create history'
      title='Create new history'
      bodyStyle={{borderRadius: 20}}
      cancelButtonProps={{style: {display: 'none'}}}
      okButtonProps={{size: 'large', htmlType: 'submit', form: 'new-note'}}
    />
  </div>
}

const NewNoteModal = ({onOk, ...rest}: any) => {
  const [form] = useForm()
  const onFinish = (values: any) => onOk(values)
  
  return <Modal {...rest}>
    <div style={{width: 'calc(100% - 30px)'}} className='mr-auto ml-auto'>
      <Form name='new-note' form={form} onFinish={onFinish}>
        <Form.Item name='title' required rules={[{required: true}]}>
          <Input size='large' bordered={false} placeholder='Add your title note'/>
        </Form.Item>
      </Form>
    </div>
  </Modal>
}

export default CreateHistory;
