import {Result, Spin} from 'antd';
import NoteScreen from "../../components/note";
import {SmileTwoTone} from "@ant-design/icons";
import {INoteItem} from "../../types/interfaces";
import React, {useEffect, useState} from 'react';
import Aside from "../../components/aside/aside";
import Navbar from "../../components/navbar/navbar";
import useStateRedux from "../../hooks/useStateRedux";
import CreateHistory from "../../components/createHistory/createHistory";

const JournalPage = () => {
  const {note: {notes, active}, UI: {secondLoading, actions: {active: current}}} = useStateRedux();
  const [state, setState] = useState<any>(null)
  
  useEffect(() => {
    if (!active) return
    const note = current !== 1 ? null : notes.find((i: INoteItem) => i.id === active)
    setState(!!note ? note : null);
  }, [active, setState, notes, current])
  
  return <div className='journal___wrapper-main-content'>
    <div className='journal__main-content'>
      <Aside/>
      <CreateHistory/>
      <div className='journal__body'>
        <Navbar/>
        <div className='journal__content beauty-scroll'>
          {secondLoading && <SaveSpinner/>}
          {
            (!active && current === 1) && (!notes.length) ? <div className='journal__no-result'>
                <Result
                  style={{marginTop: -20}}
                  icon={<SmileTwoTone twoToneColor='#5C62C5'/>}
                  title="Great, we have done all the operations!"
                />
              </div>
              :
              <NoteScreen {...state} current={current}/>
          }
        </div>
      </div>
    </div>
  </div>
}

const SaveSpinner = () => <div className='journal__loading-save animate__animated animate__fadeInDown animate__faster'>
  <Spin/> <span className='ml-1'>Saving</span>
</div>

export default JournalPage;
