import {useEffect} from "react";
import {notification} from "antd";
import {useDispatch} from "react-redux";
import {removeFormError} from "../../../actions/formValidation";

const useAlertError = (state: any, formName: string) => {
  const dispatch = useDispatch();
  useEffect(() => {
    if (!state) return
    if (!state[formName]) return
    notification.error({
      message: "Something's wrong",
      description: state[formName]
    })
    setTimeout(() => {
      dispatch(removeFormError({formName}))
    }, 500)
  }, [state, dispatch, formName])
}

export default useAlertError
