import React from 'react';
import {Link} from "react-router-dom";
import {layout} from "../../loginPage";
import {Button, Form, Input} from "antd";
import {useForm} from "antd/es/form/Form";
import formNames from "../../../../types/formNames";
import {useDispatch, useSelector} from "react-redux";
import useAlertError from "../../hooks/useAlertError";
import GoogleButton from "../SocialButton/GoogleButton";
import {startGoogleLogin} from "../../../../actions/auth";

interface IProps {
  onFinish: (values: any, formName: string) => void
}

const LoginForm = ({onFinish}: IProps) => {
  const [form] = useForm()
  const dispatch = useDispatch();
  const formName = formNames.login;
  const state: any = useSelector(state => state);
  useAlertError(state?.form || {}, formName)
  const googleLogin = () => dispatch(startGoogleLogin(formName));
  return <div>
    <Form name={formName} form={form} onFinish={values => onFinish(values, formName)} {...layout}>
      <Form.Item label='Username or Email' name='email' rules={[{required: true}]} required>
        <Input size='large' placeholder='example@mail.com'/>
      </Form.Item>
      <Form.Item style={{marginBottom: 0}} label='Password' name='password' rules={[{required: true}]} required>
        <Input.Password placeholder='* * * *' size='large'/>
      </Form.Item>
      <div style={{textAlign: 'right'}} className='mb-1'>
        <Link to='/'>
          <Button style={{padding: 0, fontSize: '12px'}} type='link'>Forgot your password?</Button>
        </Link>
      </div>
      <Form.Item>
        <Button
          size='large'
          type='primary'
          htmlType='submit'
          loading={state.UI.secondLoading}
          disabled={state.UI.secondLoading}
          className='auth__btn-login'>Login</Button>
      </Form.Item>
      <Form.Item>
        <div style={{display: 'flex', flexWrap: 'wrap'}} className='mt-1'>
          <GoogleButton onClick={googleLogin}/>
        </div>
      </Form.Item>
    </Form>
  </div>
}

export default LoginForm;
