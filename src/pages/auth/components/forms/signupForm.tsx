import React from 'react';
import {layout} from "../../loginPage";
import {Button, Form, Input} from "antd";
import {useForm} from "antd/es/form/Form";
import formNames from "../../../../types/formNames";
import {useDispatch, useSelector} from "react-redux";
import useAlertError from "../../hooks/useAlertError";
import GoogleButton from "../SocialButton/GoogleButton";
import {startGoogleLogin} from "../../../../actions/auth";

const SignupForm = ({onFinish}: IProps) => {
  const [form] = useForm()
  const formName = formNames.signup;
  const dispatch = useDispatch();
  const state: any = useSelector(state => state)
  useAlertError(state?.form || {}, formName)
  const googleRegister = () => dispatch(startGoogleLogin(formName))
  
  return <div>
    <Form name={formName} {...layout} form={form} onFinish={(v) => onFinish(v, formName)}>
      <Form.Item label='Full Name' name='name' rules={[{required: true}]}>
        <Input size='large' placeholder='Ex. Jhon A.'/>
      </Form.Item>
      <Form.Item label='Username or Email' name='email' rules={[{type: 'email'}, {required: true}]} required>
        <Input size='large' placeholder='example@mail.com'/>
      </Form.Item>
      <Form.Item label='Password' name='password' rules={[{required: true}]} required>
        <Input.Password size='large' placeholder='* * * *'/>
      </Form.Item>
      <Form.Item>
        <Button
          size='large'
          type='primary'
          htmlType='submit'
          disabled={state.UI.secondLoading}
          loading={state.UI.secondLoading}
          className='auth__btn-login'>Signup</Button>
      </Form.Item>
      <Form.Item>
        <GoogleButton onClick={googleRegister}/>
      </Form.Item>
    </Form>
  </div>
}

interface IProps {
  onFinish: (values: any, formName: string) => void
}

export default SignupForm;
