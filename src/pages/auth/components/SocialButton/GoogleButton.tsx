import React from 'react';
import './_socialNetwork.scss'

const GoogleButton = ({onClick}: IProps) => {
  return <div className="social__google-btn" onClick={onClick}>
    <div>
      <span className="icon" style={{background: `url('${process.env.PUBLIC_URL}/assets/icons/google_icon.svg')`}}/>
      <span className="buttonText">Google</span>
    </div>
  </div>
}

interface IProps {
  onClick: () => void
}

export default GoogleButton;
