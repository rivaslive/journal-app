import React from 'react';
import Icon from '@ant-design/icons'
import './_socialNetwork.scss'
import {Button} from "antd";
import {FacebookSvg} from "../../../../helpers/facebook";

const Facebook = (props: any) => <Icon component={FacebookSvg} {...props}/>

const FacebookButton = (props: IProps) => {
  return <Button
    size='large'
    icon={<Facebook />}
    style={{backgroundColor: '#1877F2', color: "white", width: '100%'}}>Login</Button>
}

interface IProps {

}

export default FacebookButton;
