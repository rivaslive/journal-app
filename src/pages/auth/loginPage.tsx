import React from 'react';
import {Link} from 'react-router-dom';
import {useDispatch} from "react-redux";
import LoginForm from "./components/forms/loginForm";
import {startEmailAndPasswordLogin} from "../../actions/auth";
import {Typography} from "antd";

const {Title} = Typography;

export const layout = {
  labelCol: {span: 24},
  wrapperCol: {span: 24},
};

const LoginPage = () => {
  const dispatch = useDispatch();
  const onFinish = (values: any, formName: string) => dispatch(startEmailAndPasswordLogin({...values, formName}))
  
  return <div className='auth__content-wrapper animate__animated animate__jackInTheBox'>
    <Title level={3}>Login</Title>
    <LoginForm onFinish={onFinish}/>
    <div style={{display: 'flex', justifyContent: 'center'}}>
      <p className='font-size-12'>Don't have an account yet?</p>
      <Link to='/auth/signup' className='ml-1 font-size-12'>Register Now</Link>
    </div>
  </div>
}


export default LoginPage;
