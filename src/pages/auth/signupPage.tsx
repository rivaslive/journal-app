import React from 'react';
import {Typography} from "antd";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import SignupForm from "./components/forms/signupForm";
import {registerWithEmailAndPassword} from "../../actions/auth";

const {Title} = Typography;

const SignupPage = () => {
  const dispatch = useDispatch();
  const onFinish = (values: any, formName: string) => dispatch(registerWithEmailAndPassword({...values, formName}))
  
  return <div className='auth__content-wrapper animate__animated animate__jackInTheBox'>
    <Title level={3}>Signup</Title>
    <SignupForm onFinish={onFinish}/>
    <div style={{display: 'flex', justifyContent: 'center'}}>
      <p className='font-size-12'>You have an account yet?</p>
      <Link to='/auth/login' className='ml-1 font-size-12'>Login</Link>
    </div>
  </div>
}

export default SignupPage;
