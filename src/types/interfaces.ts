export interface IStateRedux {
  auth: AuthState
  notes: NotesState
  form: any
  UI: UIState
}

export interface UIState {
  loading: boolean,
  actions: {
    data: {id: number, name: string}[],
    active: number | string
  },
  secondLoading: boolean
  notification: INotification
}

export interface INotification {
  show: boolean,
  description: string,
  message: string,
  type: 'success' | 'error' | 'warning' | 'info'
}

export interface INotificationPayload {
  show?: boolean,
  description?: string,
  message?: string,
  type?: 'success' | 'error' | 'warning' | 'info'
}

export interface ILogin {
  formName: string,
  errors?: {
    name: string,
    errors: string[]
  }[] | string
}

export interface AuthState {
  isAuth: boolean
  uid?: string
  name?: string
  avatar?: string
}

export interface NotesState {
  notes: INoteItem[]
  active: string | null
}

export interface INoteItem {
  id?: string
  title: string
  note?: string
  image?: string
  date?: string
}

export interface IActionType {
  type: string
  payload?: any
}
