export const types = {
  // UI actions
  uiChangeLoading: '[UI] !loading',
  uiChangeSecondLoading: '[UI] !secondLoading',
  uiSendNotification: '[UI] create notification',
  uiRemoveNotification: '[UI] remove notification',
  uiHandleActiveAction: '[UI] active action',
  
  // Auth action
  login: '[auth] login',
  logout: '[auth] logout',
  
  // form error
  formError: '[form] setError',
  formRemoveError: '[form] setRemoveError',
  
  // notes
  setNote: '[notes] set notes',
  createNote: '[notes] create',
  updateNote: '[notes] update',
  deleteNote: '[notes] delete',
  notesClean: '[notes] notes clean',
  handleActiveNote: '[notes] active note',
}
