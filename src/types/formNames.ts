const formNames = {
  login: 'formLogin',
  signup: 'formSignup',
}

export default formNames
