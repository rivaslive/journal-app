import firebase from "firebase/app";
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyDPazt3ajbdAUYHQw5f1-6P0jE3Dn2P8w8",
  authDomain: "journal-app-9c15b.firebaseapp.com",
  projectId: "journal-app-9c15b",
  storageBucket: "journal-app-9c15b.appspot.com",
  messagingSenderId: "305189637312",
  appId: "1:305189637312:web:a07ead1e6b0c2665a4b125"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const storage = firebase.storage;
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export {
  firebase,
  storage,
  db,
  googleAuthProvider
}
