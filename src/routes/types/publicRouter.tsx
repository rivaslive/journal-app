import React from 'react';
import {Redirect, Route} from "react-router-dom";
import useStateRedux from "../../hooks/useStateRedux";

const PublicRouter = ({component: Component, ...rest}: IProps) => {
  const {auth} = useStateRedux()
  return <Route {...rest} render={(props: any) => (
    !auth.isAuth ?
      <Component {...props} />
      : <Redirect to="/dashboard"/>
  )}/>
}


interface IProps {
  component: React.ReactNode | any
  
  [key: string]: unknown
}

export default PublicRouter;
