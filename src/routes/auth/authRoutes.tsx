import React from 'react';
import PublicRouter from "../types/publicRouter";
import {Redirect, Switch} from "react-router-dom";
import {LoginPage, SignupPage} from "../../pages";

const AuthRoutes = () => {
  return <div className='auth__main'>
    <div className='auth__brand' style={{background: `url('${process.env.PUBLIC_URL}/logo.svg')`}}/>
    <div className='auth__wrapper-image'>
      <div>
        <div className='auth__content'>
          <Switch>
            <PublicRouter component={LoginPage} path='/auth/login' exact/>
            <PublicRouter component={SignupPage} path='/auth/signup' exact/>
            <Redirect to='/auth/login'/>
          </Switch>
        </div>
      </div>
    </div>
  </div>
}

export default AuthRoutes;
