import {notification} from "antd";
import {login} from "../actions/auth";
import AuthRoutes from "./auth/authRoutes";
import {firebase} from "../service/firebase";
import {IStateRedux} from "../types/interfaces";
import {cleanNotes, setInitialNotes} from "../actions/note";
import {removeNotification} from "../actions/ui";
import React, {useEffect, useState} from 'react';
import PrivateRoutes from "./private/privateRoutes";
import {useDispatch, useSelector} from "react-redux";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import LoadingScreen from "../components/loading/loadingScreen";

const AppRoutes = () => {
  const dispatch = useDispatch();
  const state = useSelector((state: IStateRedux) => state.UI)
  const [loading, setLoading] = useState(true)
  
  useEffect(() => {
    firebase.auth().onAuthStateChanged(async (user: any) => {
      if (user?.uid) {
        dispatch(login(user))
        dispatch(setInitialNotes(user.uid))
      } else dispatch(cleanNotes());
      setLoading(false)
    })
  }, [dispatch, loading])
  
  useEffect(() => {
    if (!state?.notification?.show) return
    notification[state.notification.type]({...state.notification})
    setTimeout(() => dispatch(removeNotification()), 500)
  }, [state?.notification, dispatch])
  
  if (loading) return <LoadingScreen/>
  
  return <BrowserRouter>
    <Switch>
      <Route component={AuthRoutes} path='/auth'/>
      <Route component={PrivateRoutes} path='/'/>
    </Switch>
  </BrowserRouter>
}


export default AppRoutes;
