import React from 'react';
import {JournalPage} from "../../pages";
import {Redirect, Switch} from "react-router-dom";
import PrivateRouter from "../types/privateRouter";

const PrivateRoutes = () => {
  return <Switch>
    <PrivateRouter component={JournalPage} path='/dashboard' exact/>
    <PrivateRouter component={JournalPage} path='/dashboard/:type' exact/>
    <Redirect to="/dashboard"/>
  </Switch>
}

export default PrivateRoutes;
