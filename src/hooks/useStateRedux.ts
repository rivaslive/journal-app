import {useSelector} from "react-redux";
import {AuthState, IStateRedux, NotesState, UIState} from "../types/interfaces";

const useStateRedux = (action?: 'auth' | 'note' | 'form' | 'UI') => {
  const state: any = useSelector(state => state)
  let data: IStateRedux | AuthState | NotesState | UIState | any = state
  if (!!state) {
    data = !!action ? state[action] : state
  }
  return data
}

export default useStateRedux;
