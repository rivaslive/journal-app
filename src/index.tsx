import App from './App';
import React from 'react';
import moment from "moment";
import ReactDOM from 'react-dom';
import 'animate.css/animate.min.css'
import 'antd/dist/antd.min.css'
import './styles/global.scss';

moment.locale('en_US');

ReactDOM.render(
    <App />,
  document.getElementById('root')
);
