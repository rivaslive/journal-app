import {types} from "../types/types";
import {firebase, googleAuthProvider} from "../service/firebase";
import {setFormError} from "./formValidation";
import {changeSecondLoading} from "./ui";
import {changeActiveNote} from "./note";

export const startGoogleLogin = (formName: string) => {
  return (dispatch: any) => {
    firebase.auth().signInWithPopup(googleAuthProvider)
    .then(({user}: any) => {
      dispatch(login(user))
    }).catch(({message}) => dispatch(sendError({message, formName})))
  }
}

export const startEmailAndPasswordLogin = ({email, password, formName}: any) => {
  return (dispatch: any) => {
    dispatch(changeSecondLoading())
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(({user}: any) => {
      dispatch(login(user))
      dispatch(changeSecondLoading())
    }).catch(({message}) => {
      dispatch(sendError({message, formName}))
      dispatch(changeSecondLoading())
    })
  }
}

export const registerWithEmailAndPassword = ({email, password, name, formName}: any) => {
  return (dispatch: any) => {
    dispatch(changeSecondLoading())
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(async ({user}: any) => {
      await user?.updateProfile({
        displayName: name,
        photoURL: 'https://p7.hiclipart.com/preview/802/786/502/google-account-google-search-customer-service-google-logo-login-button.jpg'
      })
      dispatch(login({...user, displayName: name}))
    }).catch(({message}) => {
      dispatch(sendError({message, formName}))
      dispatch(changeSecondLoading())
    })
  }
}

export const login = (payload: ILogin) => {
  return {
    type: types.login,
    payload
  }
}

export const logoutApp = () => {
  return async (dispatch: any) => {
    await firebase.auth().signOut()
    dispatch(changeActiveNote(''))
    dispatch(logout())
  }
}

const logout = () => {
  return {
    type: types.logout
  }
}

const sendError = ({message, formName}: ISendError) => setFormError({formName, errors: message})

interface ISendError {
  message: string
  formName: string
}

interface ILogin {
  uid: string,
  displayName: string
}
