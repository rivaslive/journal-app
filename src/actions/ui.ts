import {types} from "../types/types";
import {INotificationPayload} from "../types/interfaces";

export const changeLoading = () => {
  return {type: types.uiChangeLoading}
}
export const changeSecondLoading = () => {
  return {type: types.uiChangeSecondLoading}
}
export const sendNotification = (payload: INotificationPayload) => {
  return {type: types.uiSendNotification, payload}
}
export const removeNotification = () => {
  return {type: types.uiRemoveNotification}
}
export const handleActiveAction = (payload: string | number) => {
  return {type: types.uiHandleActiveAction, payload}
}
