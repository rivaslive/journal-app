import moment from "moment";
import {types} from "../types/types";
import {changeSecondLoading, handleActiveAction, sendNotification} from "./ui";
import {db} from "../service/firebase";
import {INoteItem, IStateRedux} from "../types/interfaces";
import {loadNotes} from "../helpers/loadNotes";

const initState = {
  note: '',
  image: '',
}

// export const addNewNote = (body: INoteItem) => {
export const addNewNote = (payload: INoteItem) => {
  return async (dispatch: any, getState: () => IStateRedux) => {
    const {uid} = getState().auth
    const body = {...initState, ...payload, date: moment().toISOString()}
    const doc = await db.collection(`${uid}/journal/notes`).add(body)
    if (!!doc?.id) {
      dispatch(addNote({...body, id: doc.id}))
      dispatch(handleActiveAction(1))
    }
    else dispatch(sendNotification({message: "Sorry!", description: "Error firebase"}))
  }
}

export const setInitialNotes = (id: string) => async (dispatch: any) => {
  const notes = await loadNotes(id)
  dispatch(setNote({notes}))
};

export const handleNote = (payload: INoteItem) => async (dispatch: any, state: any) => {
  dispatch(changeSecondLoading())
  const {uid} = state().auth;
  const payloadToFirestore = {...payload}
  delete payloadToFirestore.id
  try {
    await db.doc(`${uid}/journal/notes/${payload.id}`).update(payloadToFirestore)
    dispatch(changeNoteAction(payload))
  } catch (err) {
    dispatch(sendNotification({
      type: 'error',
      message: 'it seems that something went wrong',
      description: 'we are having problems, please retry later'
    }))
  }
  dispatch(changeSecondLoading())
};

export const deleteNote = (payload: string) => async (dispatch: any, state: any) => {
  const {uid} = state().auth;
  try {
    await db.doc(`${uid}/journal/notes/${payload}`).delete()
    dispatch(deleteNoteAction(payload))
  } catch (err) {
    dispatch(sendNotification({
      type: 'error',
      message: 'it seems that something went wrong',
      description: 'we are having problems, please retry later'
    }))
  }
};

export const changeNoteAction = (payload: any) => ({type: types.updateNote, payload});
export const deleteNoteAction = (payload: string) => ({type: types.deleteNote, payload});

export const setNote = (payload: any) => ({type: types.setNote, payload});

export const addNote = (payload: INoteItem) => ({type: types.createNote, payload});
export const cleanNotes = () => ({type: types.notesClean});

export const changeActiveNote = (payload: string) => ({type: types.handleActiveNote, payload});
