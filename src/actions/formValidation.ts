import {types} from "../types/types";
import {ILogin} from "../types/interfaces";

export const setFormError = (payload: ILogin) => {
  return {
    type: types.formError,
    payload
  }
}

export const removeFormError = (payload: ILogin) => {
  return {
    type: types.formRemoveError,
    payload
  }
}

