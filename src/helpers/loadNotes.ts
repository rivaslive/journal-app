import {db} from "../service/firebase";

export const loadNotes = async (uid: string) => {
  const docs = await db.collection(`${uid}/journal/notes`).get();
  let notes: any[] = [];
  docs.forEach(doc => {
    notes = [...notes, {id: doc.id, ...doc.data()}]
  })
  console.log(notes);
  return notes
}
