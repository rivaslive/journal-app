import React from "react";
import {CarryOutOutlined} from "@ant-design/icons";

export const transformNoteList = (notes: any) => notes.map((i: any) => ({
  title: i.title,
  key: i.id,
  icon: <CarryOutOutlined/>
}))
