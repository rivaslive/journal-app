import {getStore} from "./store";

const isLogin = () => !!getStore('auth');


export default isLogin;
