import React from 'react';
import {Provider as ProviderRedux} from 'react-redux';
import {store} from "./store/store";
import AppRoutes from './routes/AppRoutes';

function App() {
  return (
    <ProviderRedux store={store}>
      <AppRoutes/>
    </ProviderRedux>
  );
}

export default App;
