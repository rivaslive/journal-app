import {applyMiddleware, combineReducers, createStore, compose} from 'redux'
import {authReducer, formValidateReducer, noteReducer, UIReducer} from "../reducers";
import thunk from "redux-thunk";

const reducers = combineReducers({
  UI: UIReducer,
  note: noteReducer,
  auth: authReducer,
  form: formValidateReducer
})

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(reducers, composeEnhancers(
  applyMiddleware(thunk)
))
