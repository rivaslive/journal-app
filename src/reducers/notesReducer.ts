import {types} from "../types/types";
import {IActionType, NotesState} from "../types/interfaces";

/**
 * State init
 * {
 *   notes: [],
 *   active: string | null
 * }
 */

const INIT_STATE = {
  notes: [],
  active: ''
}

const notesReducer = (state: NotesState = INIT_STATE, action: IActionType) => {
  switch (action.type) {
    case types.setNote:
      return {...state, ...action.payload}
    
    case types.createNote:
      return {...state, notes: [...state.notes, action.payload], active: action.payload.id}
    
    case types.updateNote:
      return {
        ...state,
        notes: state.notes.map((i) => ((i.id !== action.payload.id) ? i : {...i, ...action.payload}))
      }
    
    case types.handleActiveNote:
      return {...state, active: action.payload}
    
    case types.deleteNote:
      return {...state, notes: state.notes.filter(i => i.id !== action.payload), active: ''}
    
    case types.notesClean:
      return INIT_STATE
    
    default:
      return state;
  }
}

export default notesReducer
