import {types} from "../types/types";
import formNames from "../types/formNames";
import {ILogin} from "../types/interfaces";

/**
 * State
 * {
 *   formLogin: [
 *     {
 *       name: string,
 *       errors: string[]
 *     }
 *   ]
 * }
 *
 * payload
 * {
 *   formName: string
 *   errors: [
 *     {
 *       name: string,
 *       errors: string[]
 *     }
 *   ]
 * }
 */

/**
 * Inicializa todos los formularios que funcionan con este reducer,
 * es necesario para no hacer mutabilidad
 **/
const INIT_STATE = () => {
  let init = {}
  const values = Object.values(formNames)
  values.forEach(val => init = {...init, [val]: ''})
  return init
}

const formValidateReducer = (state: IState | {} = INIT_STATE(), action: IAction) => {
  switch (action.type) {
    case types.formError:
      return {...state, [action.payload.formName]: action.payload.errors}
    case types.formRemoveError:
      return {...state, [action.payload.formName]: ''}
    
    default:
      return state;
  }
}

interface IState {
  [key: string]: string | {
    name: string,
    errors: string[]
  }[]
}

interface IAction {
  payload: ILogin
  type: string
}

export default formValidateReducer
