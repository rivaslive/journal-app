import {types} from "../types/types";
import {AuthState} from "../types/interfaces";

/**
 * State init
 * {
 *   uid: number
 *   name: string
 * }
 */

const INIT_AUTH = {
  isAuth: false
}

const authReducer = (state: AuthState = INIT_AUTH, action: any) => {
  switch (action.type) {
    case types.login:
      return {
        isAuth: true,
        uid: action.payload.uid,
        email: action.payload.email,
        name: action.payload.displayName,
        avatar: action.payload.photoURL
      }
    case types.logout:
      return INIT_AUTH
    
    default:
      return state;
  }
}

export default authReducer
