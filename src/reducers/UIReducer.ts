import {types} from "../types/types";
import {IActionType, UIState} from "../types/interfaces";

/**
 * State init
 * {
 *   loading: boolean
 * }
 */
const INIT_STATE: UIState = {
  loading: false,
  secondLoading: false,
  actions: {
    data: [{id: 1, name: 'Today'}, {id: 2, name: 'New Note'}],
    active: 1
  },
  notification: {
    show: false,
    description: '',
    message: 'Congratulations!',
    type: 'success'
  }
}
const UIReducer = (state: UIState = INIT_STATE, action: IActionType) => {
  switch (action.type) {
    case types.uiChangeLoading:
      return {...state, loading: !state.loading}
    
    case types.uiChangeSecondLoading:
      return {...state, secondLoading: !state.secondLoading}
    
    case types.uiSendNotification:
      return {...state, notification: {...state.notification, show: true, ...action.payload}}
    
    case types.uiRemoveNotification:
      return {...state, notification: {...INIT_STATE.notification, show: false}}
      
    case types.uiHandleActiveAction:
      return {...state, actions: {...state.actions, active: action.payload}}
    
    default:
      return state;
  }
}

export default UIReducer
